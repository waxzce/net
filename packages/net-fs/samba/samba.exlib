# Copyright 2011-2012 Pierre Lejeune <superheron@gmail.com>
# Copyright 2013-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pam wafsamba systemd-service

export_exlib_phases pkg_pretend src_prepare src_configure src_install

SUMMARY="Samba provides seamless file and print services to SMB/CIFS clients, e. g. Windows"
DESCRIPTION="
Samba is a free software implementation of the SMB/CIFS networking protocol that
can be run on many platforms other than Microsoft Windows. Samba uses the TCP/IP
protocol that is installed on the host. When correctly configured, it allows
that host to interact with a Microsoft Windows client or server as if it is a Windows
file and print server. Additionally, it can act as a PDC, a domain member server
or as a part of an Active Directory.
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="mirror://samba/stable/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    acl
    addc [[ description = [ Allow Samba to act as an Active Directory Domain Controller ] ]]
    ads [[
        description = [ Enable Active Directory support ]
        requires = [ acl ldap winbind ]
    ]]
    avahi [[ description = [ Enable Avahi support ] ]]
    cluster [[ description = [ Enable the Samba cluster extensions ] ]]
    cups
    gpg [[
        description = [ Store cleartext passwords in a PGP/OpenGPG encrypted form ]
        requires = [ addc ]
    ]]
    ldap
    lmdb [[ description = [ Enable LMDB backend for LDB ] ]]
    quotas [[ description = [ Enables support for user quotas ] ]]
    syslog [[ description = [ Enables support for syslog ] ]]
    system-krb5 [[ description = [ Use krb5 instead of the bundled heimdal ] ]]
    systemd
    winbind [[ description = [ Enables support for the winbind auth daemon ] ]]
"

# The test are simply broken.
RESTRICT="test"

# TODO: package liburing for vfs_io_uring (automagic)
# !system-krb5? ( app-crypt/heimdal[>=1.5] )
DEPENDENCIES="
    build:
        net-libs/rpcsvc-proto
        !system-krb5? ( sys-devel/flex )
    build+run:
        app-arch/libarchive[>=3.1.2]
        dev-db/ldb[>=2.3.0][lmdb?][python_abis:*(-)?]
        dev-db/tdb[>=1.4.3][python_abis:*(-)?]
        dev-lang/perl:=
        dev-libs/gnutls[>=3.6.15]
        dev-libs/icu:=
        dev-libs/iniparser
        dev-libs/jansson [[ note = [ JSON auth audit support ] ]]
        dev-libs/libbsd [[ note = [ automagic ] ]]
        dev-libs/libgcrypt
        dev-libs/libgpg-error
        dev-libs/libtasn1[>=3.8] [[ note = [ automagic in lib/mscat ] ]]
        dev-libs/popt
        dev-libs/talloc[>=2.3.2][python_abis:*(-)?]
        dev-libs/tevent[>=0.10.2][python_abis:*(-)?]
        dev-perl/Parse-Yapp
        dev-util/subunit[python_abis:*(-)?]
        net-libs/cyrus-sasl
        sys-apps/attr
        sys-apps/dbus
        sys-fs/e2fsprogs
        sys-libs/libcap
        sys-libs/ncurses
        sys-libs/pam
        sys-libs/readline:=
        sys-libs/zlib[>=1.2.3]
        acl? ( sys-apps/acl )
        ads? ( dev-python/dnspython[python_abis:*(-)?] )
        avahi? ( net-dns/avahi )
        cluster? ( dev-libs/ctdb )
        cups? ( net-print/cups )
        gpg? (
            app-crypt/gpgme
            dev-python/pygpgme[python_abis:*(-)?]
        )
        ldap? ( net-directory/openldap )
        syslog? ( virtual/syslog )
        system-krb5? ( app-crypt/krb5[>=1.15.1] )
        systemd? ( sys-apps/systemd )
    test:
        dev-python/iso8601[python_abis:*(-)?]
        dev-util/cmocka[>=1.1.3]
        sys-libs/nss_wrapper[>=1.1.11]
        sys-libs/pam_wrapper[>=1.1.2]
        sys-libs/resolv_wrapper[>=1.1.7]
        sys-libs/socket_wrapper[>=1.3.3]
        sys-libs/uid_wrapper[>=1.2.7]
    suggestion:
        app-antivirus/clamav [[
            description = [ vfs_virusfilter can use ClamAV to scan for viruses ]
        ]]
        media-gfx/graphviz [[
            description = [ samba-tool utilizes Graphviz for dot output and text-based heatmaps ]
        ]]
"

WAF_SRC_CONFIGURE_PARAMS+=(
    --builtin-libraries=NONE
    --nopyc
    --nopyo
    --sbindir=/usr/$(exhost --target)/bin
    --systemd-install-services
    --enable-fhs
    --enable-pthreadpool
    --disable-cephfs
    --disable-glusterfs
    --disable-spotlight
    --disable-warnings-as-errors
    --with-iconv
    --with-json
    --with-libarchive
    --with-lockdir=/run/lock/samba
    --with-modulesdir=/usr/$(exhost --target)/lib/samba
    --with-pam
    --with-pammodulesdir=$(getpam_mod_dir)
    --with-piddir=/run
    --with-privatedir=/var/lib/${PN}/private
    --with-sendfile-support
    --with-sockets-dir=/run/samba
    --with-systemddir=${SYSTEMDSYSTEMUNITDIR}
    --with-utmp
    --without-automount
    --without-dmapi
    --without-fam
    --without-lttng
    --without-ntvfs-fileserver
)

WAF_SRC_CONFIGURE_OPTION_ENABLES=(
    avahi
    cups
)

WAF_SRC_CONFIGURE_OPTION_WITHS=(
    "acl acl-support"
    ads
    "ads dnsupdate"
    "cluster cluster-support"
    "gpg gpgme"
    ldap
    "lmdb ldb-lmdb"
    quotas
    syslog
    systemd
    winbind
)

WAF_SRC_CONFIGURE_TESTS=(
    --enable-selftest
)

# New for samba-4.10.x:
# Allow parallel execution of the install phase to minimise build times.
# Has been tested with no ill effects up to --jobs=16.
WAF_SRC_INSTALL_PARAMS+=( --jobs=${EXJOBS} )

samba_pkg_pretend() {
    if [[ ! "${EXJOBS}" = 1 ]] ; then
        ewarn "Building Samba with jobs=n (n>1) is known to fail for double digit values of n."
        ewarn "If you experience a build failure, only report it after reproducing it with jobs=1."
        ewarn "In practice, there isn't much value in going beyond jobs=9. YMMV."
    fi
}

samba_src_prepare() {
    default

    # adjust to /run
    edo sed \
        -e "s:/var/run:/run:g" \
        -i packaging/systemd/samba.conf.tmp

    # rpcgen hardcodes the cpp command
    local dir=${WORKBASE}/symlinked-build-tools
    edo mkdir -p ${dir}
    edo ln -s /usr/host/bin/$(exhost --target)-cpp ${dir}/cpp
    edo sed \
        -e "s:rpcgen:rpcgen -Y ${WORKBASE}/symlinked-build-tools:g" \
        -i source3/modules/wscript_build \
        -i ctdb/wscript

    # glibc 2.30 removed <stropts.h>, last checked: 4.14.2
    edo sed \
        -e '/HAVE_STROPTS_H/,+2 d' \
        -i lib/replace/system/network.h

#    # ensure system heimdal is used
#    if ever at_least 4.3.0; then
#        edo rm source4/heimdal_build/krb5-types.h
#        edo rm source4/heimdal/kdc/kdc.h
#    fi
}

samba_src_configure() {
    WAF_SRC_CONFIGURE_PARAMS+=(
        $(option system-krb5 && echo --with-system-mitkrb5)
        $(option system-krb5 && echo --with-experimental-mit-ad-dc)
        $(option !addc && echo --without-ad-dc)
    )

    # Samba doesn't build with heimdal 1.5.3, 1.6_rc2 or git master
    if ! option system-krb5; then
        WAF_SRC_CONFIGURE_PARAMS+=(
            --bundled-libraries=NONE,roken,wind,hx509,asn1,heimbase,hcrypto,krb5,gssapi,heimntlm,hdb,kdc,com_err,compile_et,asn1_compile
        )
    else
        WAF_SRC_CONFIGURE_PARAMS+=(
            --bundled-libraries=NONE
        )
    fi

    waf_src_configure
}

samba_src_install() {
    waf_src_install

    python_bytecompile

    option addc || edo rm "${IMAGE}"/${SYSTEMDSYSTEMUNITDIR}/samba.service
    option winbind || edo rm "${IMAGE}"/${SYSTEMDSYSTEMUNITDIR}/winbind.service

    # tmpfile is included but doesn't get installed, last checked: 4.8.0
    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/samba 0755 root root -
EOF

    if option cluster; then
        insinto /usr/$(exhost --target)/lib/tmpfiles.d
        hereins ctdb.conf <<EOF
d /run/ctdb 0755 root root -
EOF
    fi

    if option cups; then
        dodir $(cups-config --serverbin)/backend
        dosym /usr/$(exhost --target)/bin/smbspool $(cups-config --serverbin)/backend/smb
    fi

    keepdir /var/{cache,lib,log}/samba
    edo rmdir "${IMAGE}"/run/{lock/{samba,},{samba,}}

    keepdir /var/lib/samba/bind-dns
    keepdir /var/lib/samba/private/smbd.tmp/messaging

    insinto /etc/${PN}
    doins "${WORK}"/examples/smb.conf.default
}

