# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.gz ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require python [ blacklist=2 multibuild=false ]
require systemd-service
require bash-completion zsh-completion
require gsettings

export_exlib_phases src_install

SUMMARY="A firewall daemon with D-Bus interface providing a dynamic firewall"
DESCRIPTION="
Firewalld provides a dynamically managed firewall with support for
network/firewall zones that define the trust level of network connections or
interfaces. It has support for IPv4, IPv6 firewall settings, ethernet bridges
and IP sets. There is a separation of runtime and permanent configuration
options. It also provides an interface for services or applications to add
firewall rules directly."

HOMEPAGE+=" https://firewalld.org/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="systemd"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-libs/glib:2
        dev-libs/libxslt
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        systemd? ( sys-apps/systemd )
    run:
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/decorator[python_abis:*(-)?]
        dev-python/python-slip[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        net-firewall/ipset
        net-firewall/iptables
        net-firewall/nftables[python][python_abis:*(-)?]
"

UPSTREAM_DOCUMENTATION="https://firewalld.org/documentation/"
UPSTREAM_RELEASE_NOTES="https://firewalld.org/2021/01/${PN}-${PV/./-}-release"

# Needs root
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    --disable-rpmmacros
    --disable-sysconfig
    # Needs xsltproc and docbook-xsl-stylesheets but man pages are always nice
    --enable-docs
    --with-systemd-unitdir=${SYSTEMDSYSTEMUNITDIR}
    --with-bashcompletiondir=${BASHCOMPLETIONDIR}
    --with-zshcompletiondir=${ZSHCOMPLETIONDIR}
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=(
    systemd
)

firewalld_src_install() {
    default

    keepdir /etc/${PN}/{helpers,icmptypes,ipsets,policies,services,zones}

    # Remove GUI bits, currently not interested in that, would need gtk+:3,
    # PyQt5 and some python packages (python-slip (unwritten), dbus-python,
    # decorator, pygobect:3)
    edo rm -r "${IMAGE}"/etc/xdg "${IMAGE}"/usr/share/{applications,icons}
    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/firewall-{applet,config}

    option bash-completion || edo rm -r "${IMAGE}"/usr/share/bash-completion
    option zsh-completion || edo rm -r "${IMAGE}"/usr/share/zsh
}

