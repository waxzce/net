# Copyright 2017-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="openHAB - a vendor and technology agnostic open source automation software for your home"
DESCRIPTION="
openHAB is a software for integrating different home automation systems and technologies into one
single solution that allows over-arching automation rules and that offers uniform user interfaces.
"
HOMEPAGE="https://www.openhab.org"
DOWNLOADS="${HOMEPAGE}/download/releases/org/${PN}/distro/${PN}/${PV}/${PNV}.tar.gz"

UPSTREAM_DOCUMENTATION="https://docs.openhab.org [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="https://github.com/openhab/openhab-distro/releases/tag/${PV} [[ lang = en ]]"

LICENCES="EPL-1.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        group/audio
        group/dialout
        group/tty
        group/${PN}
        user/${PN}
    run:
        virtual/jre:11.0
"

WORK=${WORKBASE}

src_test() {
    :
}

src_install() {
    insinto /etc/default
    doins "${FILES}"/${PN}
    insinto /etc/profile.d/
    doins "${FILES}"/${PN}.sh
    insinto /etc/${PN}
    doins -r conf/*

    insinto /usr/share/${PN}
    doins -r addons runtime
    exeinto /usr/share/${PN}
    doexe *.sh

    insinto /var/lib/${PN}
    doins -r userdata/*

    keepdir /var/lib/${PN}/persistence/{db4o,mapdb,rrd4j}
    keepdir /var/log/${PN}

    edo rm "${IMAGE}"/usr/share/${PN}/runtime/bin/*.{bat,ps1}
    edo rm -rf "${IMAGE}"/usr/share/${PN}/runtime/bin/contrib

    edo chmod 0755 "${IMAGE}"/usr/share/${PN}/runtime/bin/{backup,client,inc,instance,karaf,oh_dir_layout,restore,setenv,shell,start,status,stop,update}
    edo chown -R openhab:openhab "${IMAGE}"/etc/${PN}
    edo chown -R openhab:openhab "${IMAGE}"/usr/share/${PN}
    edo chown -R openhab:openhab "${IMAGE}"/var/{lib,log}/${PN}

    install_systemd_files
}

