# Copyright 2008, 2011, 2012, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic

export_exlib_phases pkg_setup

SUMMARY="A curses BitTorrent client."
DESCRIPTION='
The client uses ncurses and is ideal for use with screen or dtach.
It supports saving of sessions and allows the user to add and remove torrents.
'
HOMEPAGE="https://rakshasa.github.io/${PN}/"
DOWNLOADS="http://rtorrent.net/downloads/${PNV}.tar.gz"

SLOT="0"
LICENCES="GPL-2"
MYOPTIONS="debug xmlrpc platform: x86"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-misc/curl[>=7.15.4]
        sys-libs/ncurses
        xmlrpc? ( dev-libs/xmlrpc-c )
    test:
        dev-cpp/cppunit
    suggestion:
        net-misc/curl[ares] [[ description = [
            RTorrent will block on hostname lookup during tracker requests and http downloads,
            unless you build net-misc/curl with ares support. ] ]]"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'xmlrpc xmlrpc-c' )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( doc/${PN}.rc )

rtorrent_pkg_setup() {
    # Known broken on x86: http://libtorrent.rakshasa.no/wiki/LibTorrentKnownIssues
    option platform:x86 && filter-flags -fomit-frame-pointer
}

