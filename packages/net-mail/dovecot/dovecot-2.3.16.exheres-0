# Copyright 2008 Fernando J. Pereda
# Copyright 2009, 2010 Daniel Mierswa <impulze@impulze.org>
# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# edo sed ... src/lib-program-client/Makefile.am
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require systemd-service

case "$(ever range 4)" in
    rc*|beta*|alpha*) MYPV="$(ever range 1-3).$(ever range 4)" ;;
    *) MYPV="${PV}" ;;
esac

case "$(ever range 4)" in
    rc*) DOWNLOAD_PATH="rc" ;;
    beta*) DOWNLOAD_PATH="beta" ;;
    alpha*) DOWNLOAD_PATH="alpha" ;;
esac

SUMMARY="Fast, secure, IMAP/POP server"
DESCRIPTION="
Dovecot is an open source IMAP and POP3 email server for Linux/UNIX-like systems,
written with security primarily in mind. Dovecot is an excellent choice for both
small and large installations. It's fast, simple to set up, requires no special
administration and it uses very little memory.
"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="${HOMEPAGE}/releases/$(ever range 1-2)/${DOWNLOAD_PATH}/${PN}-${MYPV}.tar.gz"
LICENCES="LGPL-2.1 MIT BSD-3 public-domain"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    argon2 [[ description = [ Support hashing passwords with Argon2 via libsodium ] ]]
    caps
    ldap
    lucene [[ description = [ Support for full text search with CLucene ] ]]
    lz4 [[ description = [ Build with LZ4 compression support ] ]]
    mysql
    postgresql
    solr [[ description = [ Apache Solr fast text searching backend ] ]]
    sqlite
    systemd
    tcpd
    zstd [[ description = [ Build with ZSTD compression support ] ]]
    (
        mysql
        postgresql
        sqlite
    ) [[ description = [ Database backends for dovecot and plugins ] ]]

    ( libc: musl )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

WORK=${WORKBASE}/${PN}-${MYPV}

DEPENDENCIES="
    build:
        app-text/unicode-data
        virtual/pkg-config[>=0.28]
    build+run:
        app-arch/xz
        app-arch/bzip2
        dev-libs/icu:=
        dev-libs/libunwind
        sys-libs/pam
        sys-libs/zlib
        argon2? ( dev-libs/libsodium )
        caps? ( sys-libs/libcap )
        ldap? ( net-directory/openldap )
        libc:musl? ( sys-libs/musl[>=1.1.20] [[ note = getrandom ]] )
        !libc:musl? ( sys-libs/glibc[>=2.25] [[ note = getrandom ]] )
        lucene? ( dev-cpp/clucene:1 )
        lz4? ( app-arch/lz4 )
        mysql? ( virtual/mysql )
        postgresql? ( dev-db/postgresql-client )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        solr? (
            dev-libs/expat
            net-misc/curl
        )
        sqlite? ( dev-db/sqlite:3 )
        systemd? ( sys-apps/systemd )
        tcpd? ( sys-apps/tcp-wrappers )
        zstd? ( app-arch/zstd )
        user/dovecot
        user/dovenull
    run:
        group/dovecot
        user/dovecot
    suggestion:
        net-mail/dovecot-pigeonhole
        [[ description =
            [ Sieve plugin for deliver and Managesieve implementation. ]
        ]]
        postgresql? ( dev-db/postgresql:* )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    PANDOC=false
    VALGRIND=no
    --enable-hardening
    --disable-ubsan
    --localstatedir=/var
    --with-bzlib
    --with-docs
    --with-icu
    --with-lzma
    --with-pam
    --with-rundir=/run/dovecot
    --with-shadow
    --with-ssl
    --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-zlib
    --without-cassandra
    --without-fuzzer
    --without-lua
    # broken (?)
    --without-gssapi
    # prefer getrandom from the libc instead of arc4radom from libbsd
    --without-libbsd
    --without-nss
    --without-stemmer
    --without-textcat
    # avoid automagic dependencies
    --without-apparmor
    --without-cdb
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "argon2 sodium"
    "caps libcap"
    ldap
    lucene
    lz4
    mysql
    "postgresql pgsql"
    solr
    sqlite
    systemd
    "tcpd libwrap"
    zstd
)

src_unpack() {
    default

    edo cp /usr/share/unicode-data/UnicodeData.txt "${WORK}"/src/lib
}

src_prepare() {
    # Disable test which tries to bind to 0.0.0.0@0
    edo sed \
        -e '/^test_programs =/,+2s:test-program-client-unix \\:test-program-client-unix:' \
        -e '/^test_programs =/,+3s:.*test-program-client-net::' \
        -i src/lib-program-client/Makefile.am

    edo sed \
        -e '/[[:space:]]test-http-client-errors \\/d' \
        -i src/lib-http/Makefile.am

    edo sed \
        -e '/[[:space:]]test-smtp-client-errors \\/d' \
        -i src/lib-smtp/Makefile.am

    # Disable test which fails under sydbox
    edo sed \
        -e '/^test_programs =/,+1s:test-master-service-settings-cache \\:test-master-service-settings-cache:' \
        -e '/^test_programs =/,+2s:.*test-event-stats::' \
        -i src/lib-master/Makefile.am

    # Disable test which fails due to files names getting to long
    edo sed -e '/^test_programs =/,+2d' -i src/imap/Makefile.am

    autotools_src_prepare
}

src_test() {
    esandbox allow_net "unix:${WORK}/src/lib-program-client/program-client-test.sock"
    esandbox allow_net --connect "LOOPBACK@0"
    esandbox allow_net "unix:${WORK}/src/master/auth-client-test"
    esandbox allow_net "unix:${WORK}/src/master/auth-master-test"
    esandbox allow_net "unix:${WORK}/src/master/master-login-auth-test"
    default
    esandbox disallow_net "unix:${WORK}/src/master/master-login-auth-test"
    esandbox disallow_net "unix:${WORK}/src/master/auth-master-test"
    esandbox disallow_net "unix:${WORK}/src/master/auth-client-test"
    esandbox disallow_net --connect "LOOPBACK@0"
    esandbox disallow_net "unix:${WORK}/src/lib-program-client/program-client-test.sock"
}

src_install() {
    default

    # remove pointless README which just points to /usr/share/doc/*
    edo rm "${IMAGE}"/etc/dovecot/README
    edo rmdir "${IMAGE}"/etc/dovecot

    # /etc should be empty, because init.d script won't get installed
    edo rmdir "${IMAGE}"/etc

    keepdir /usr/$(exhost --target)/lib/dovecot/auth
}

